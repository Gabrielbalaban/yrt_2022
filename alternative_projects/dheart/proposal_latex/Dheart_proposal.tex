\documentclass[a4paper,11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[margin=20mm,includehead,includefoot]{geometry}
\usepackage[usenames,dvipsnames,table]{xcolor}
%\usepackage[font=normalsize, skip=2pt]{caption}
\usepackage[labelfont=bf]{caption}
\usepackage{sectsty}
\usepackage{titlesec}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{eurosym}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{url}
\usepackage[nolist]{acronym}

\usepackage{tikz}
\usepackage{everypage}

%\setmainfont{Calibri}

\DeclareUnicodeCharacter{2060}{\nolinebreak}
\newcommand{\projname}{DHeart}
%\newcommand{\smallhead}[1]{\smallskip\noindent\textbf{#1}}
\newcommand{\paragraphtitle}[1]{\noindent\textbf{\color{blue}#1}}

%Colouring of Sections and Subsections
\subsectionfont{\color{blue}}

\titleformat{\subsubsection}[block]{\color{blue}
\bfseries\filright}{}{0em}{}

\titleformat{\subsection}[hang]{\color{blue}
\large\scshape\filright}{\thesubsection}{1em}{}

\titleformat{\section}[block]{\color{blue}\Large\bfseries\filcenter}{\thesection}{1em}{}
%\sectionfont{\color{blue}}

%Less space before and after section titles
%\titlespacing{\section}{0pt}{5pt}{-\parskip}
%\titlespacing{\subsection}{0pt}{\parskip}{-\parskip}

%Less space after figure captions
%\setlength{\belowcaptionskip}{-10pt}
%\setlength{\abovecaptionskip}{0pt}
\setlength{\textfloatsep}{4pt}
\setlength{\intextsep}{5pt}

% Get rid of spaces in bibliography
\let\OLDthebibliography\thebibliography
\renewcommand\thebibliography[1]{
  \OLDthebibliography{#1}
  \setlength{\parskip}{0pt}
  \setlength{\itemsep}{0pt plus 0.3ex}
}

\graphicspath{{../images/}}

%Logo in Header
%\fancyhead[L]{}
%\fancyhead[R]{\includegraphics[width=1.5cm]{../logo/logo.pdf}}
%\renewcommand{\headrulewidth}{0pt}
\author{}
\title{{\color{blue}\vspace{-2.0cm}
Data-Driven Biophysical Modelling with Cardiological Patient Data (\projname)} \vspace{-2.2cm}}
\date{}
\begin{document}
\maketitle

\begin{acronym}
\acro{PL}{Project Leader}
\end{acronym}

\section{Excellence}
\subsubsection{Vision}
Imagine the ability to automatically discover hidden biophysical relationships in cardiological patient data. Fueled by recent advances in machine learning, biophysical modelling, and cardiological data curation,  such data-driven biophysical discovery could radically improve our understanding of cardiac function, provide new avenues of investigation, and ultimately give new insights into deadly heart diseases. This is the vision of the \projname \ project, which will provide the key methodological foundations to take the modelling and simulation of the heart to the next new level (Figure~\ref{fig:vision}). \\
\begin{figure}[h]
\centering
\includegraphics[width=0.8\linewidth]{Fig1_project_premise_B.png}
\caption{(Left) traditional cardiac biophysics modelling pipeline in which a human modeller decides how to combine mathematical relations, physical principles, and data from animal models and tissue experiments into a computational heart model. Here, the medical validation of the resulting computational heart models is challenging due to the multiscale biophysical complexity. (Right) The \projname \ approach, which automatically designs computational heart models using patient data and machine learning. The resulting model is medically validated by design, and can be used to optimize therapies and improve patient outcomes.}
\label{fig:vision}
\end{figure}

\subsection{State of the art, knowledge needs and project objectives}
\subsubsection{State of the art, knowledge needs and challenges}
The human heart is the workhorse of the body, pumping over 8,000 litres of blood a day under healthy conditions \cite{marinelli1995heart}. This impressive performance is enabled by a sophisticated, multiscale, biophysical system of electrical excitement, contraction and relaxation, coordinated to millisecond precision. However, when the heart’s biophysical functions are compromised by disease, the results can be disastrous. Heart failure, arrhythmia, and stroke are all cardiovascular conditions  with biophysical causes, and potentially lethal results. Indeed, cardiovascular diseases are the leading cause of mortality in the world, with an estimated 17.9 million annual deaths \cite{WHO_CVD}, which is equivalent to 3 1/2 times the mortality of the entire COVID-19 pandemic each year \cite{WHO_COVID}. Many cardiovascular fatalities could be prevented if we improved our understanding of the biophysical processes that underly cardiac function and disease. However, at the moment there are major gaps in our knowledge \cite{niederer2019short}.

Our current understanding of cardiac biophysics is based heavily on data originating from isolated cell and tissue experiments. This data is often combined with basic first principles to derive mathematical models, which describe a variety of individual biophysical processes, ranging from muscle contraction \cite{land2017model} to elasticity \cite{holzapfel2009constitutive}, and ion channel dynamics \cite{ten2006alternans}. Nevertheless, the heart is an integrated organ in which biophysical processes work in tandem. For medical purposes, individual cardiac biophysics models are therefore typically combined into higher-level models of cardiac function, and applied to personalized organ geometries originating from medical imaging \cite{chabiniok2016multiphysics}. Such personalized cardiac biophysics models have seen recent successes for a variety of medical applications, ranging from arrhythmia risk stratification (Figure~\ref{fig:example_simulations}), to catheter ablation therapy planning \cite{boyle2019computationally}, resynchronization therapy optimization \cite{strocchi2020his}, and drug safety evaluation \cite{costabal2019machine}. These developments in image-based biophysical modelling are making cardiology safer and more effective by replacing invasive procedures with in-silico experimentation on patient-specific "digital twin" hearts \cite{corral2020digital}. The \projname \ \ac{PL} Gabriel Balaban has over 7 years of experience (see CV) researching in this multidisciplinary area.

\begin{wrapfigure}{r}{0.5\textwidth}
\includegraphics[width=\linewidth]{Fig2_reentrysimulation.png}
\caption{Example medical image based biophysical simulation (PL's study \cite{balaban2018fibrosis}) A) Contrast enhanced cardiac MRI showing location of damaged heart tissue. B) Electrical simulation of a dangerous reentrant arrhythmia.
Such medical image based models could be enhanced with biophysical discovery techniques to gain further insights and improved clinical utility.}
\label{fig:example_simulations}
\end{wrapfigure}

Despite recent successes, the clinical validation of organ-level cardiac biophysics models remains a major challenge \cite{niederer2019short}, which has limited the translation of biophysics models to cardiological practice. This is due to various difficulties that arise when lower-level component models are combined. \textbf{(a)} real-world cardiological scenarios often involve new or unknown biophysics that are not captured by a-priori model specifications. \textbf{(b)}  
Experimental data sets underlying component models may contain incompatibilities, including animal data which may be inapropriate for human predictions. Finally, \textbf{(c)} integrating biophysical components into higher-level models leads to increased model complexity, greater numbers of variables, and overfitting issues. 

All of the above mentioned challenges have been addressed at the cellular scale by recent advances in data-driven biophysics \cite{costello2018machine, mangan2016inferring, ayed2019ep}. In particular, Costello et al developed a method to automatically derive enzyme kinetics models from multiomics data \cite{costello2018machine}, thereby demonstrating that machine learning methods can uncover hidden biophysical relations \textbf{(a)}, and resolve discrepencies between disperate data sources \textbf{(b)}. Mangan et al developed a novel sparsity promoting approach to discovering biophysical system dynamics \cite{mangan2016inferring}, which automatically reduces model complexity and alleviates overfitting issues, thereby addressing \textbf{(c)}.

We are however, still in the infancy of data-driven cardiac biophysics \cite{peng2021multiscale}. Current studies have been restricted to the subcellular level \cite{lei2021neural}, or the reproduction of synthetic data \cite{ayed2019ep}. To the best of our knowlege, biophysical discovery techniques have not been applied at the organ scale to medical-image based computational heart models, nor used to predict or optimize patient outcomes for entire clinical cohorts. Indeed, the key technological foundations are currently lacking for these endeavours, which will require addressing a major step-change in computational complexity. Notably, highly flexible and efficient finite element software suitable for this challenge has been developed at Simula \cite{alnaes2015fenics}, the \projname \ host institute. Also, the \ac{PL} has extensive experience with scaling cardiological simulation techniques to large patient cohorts \cite{balaban2021late, balaban2019scar}.

On this background the \projname \ ambition is to \textbf{establish the technological foundations for data-driven biophysical discovery from cardiological image data, electrocardiograms and patient records, allowing for pioneering in-silico studies of data driven cardiac biophysics in large patient cohorts.}

\subsubsection{Project objectives}
(\emph{work in progress})
\begin{itemize}
\item Obective 1: To lay the methodological foundations for data-driven biophysical discovery from cardiological patient data.\\
\emph{Questions: What kind of biophysics models can be identified by what kind of data under ideal conditions? How much data do we need? What kind of algorithms can we use?}
\item Objective 2: To create a modelling platform for building and validating data-driven cardiac biophysics models for cohort data representing key patient phenotypes. \\
\emph{Questions Can we derive biophysics models with minimal complexity that are able to reproduce clinical phenotypes?
Can we create data-driven biophysical models that are able to diagnose diseases and or predict patient outcomes?}

\end{itemize}

%\subsection*{Research Methods}
%
%\paragraphtitle{Machine Learning:}
%I will leverage two key techniques to make biophysical discovery from patient data a reality: symbolic regression and neural networks. 
%Symbolic regression is a type of regression analysis that searches through a space of mathematical expressions to create a model that best fits a given dataset. It shares many characteristics with the Unified Form Language \cite{alnaes2014unified} developed at Simula, in that symbolic representations are manipulated to create a physical model. However, with symbolic regression the process is data driven, and physical relations in the final model can be learned automatically via evolutionary computing. This results in fully explicit equations that can be directly interpreted by the analyst.
%
%
%Complementary to symbolic regression are neural networks, which build function approximations by sequentialy transforming data through a series of layers, whose relationships are learned from training data. Neural networks are universal function approximators, so that there are fewer limits on what can be learned than with symbolic regression. For biophysical modelling, any modelling component that is a function can potentially be replaced by a neural network. Relevant examples are strain energy functions in mechanics, or ion channel gates in electrophysiology. \mbox{} \\
%
%\begin{wrapfigure}{r}{0.5\textwidth}
%\includegraphics[width=0.5\textwidth]{simula_research_portrait}
%\caption{Biophysical modelling at Simula across multiple spatial scales. A) Ischemic Heart Disease Arrhythmia Simulation \cite{arevalo2016arrhythmia} B) Brain Tracer Simulation \cite{croci2021fast} C) Carotid Artery Blood Flow \cite{bergersen2020framework} D) Pulmonary Vein Re-entry \cite{jaeger2021mutations} E) Fibrosis Mediated Re-entry \cite{balaban2021late} F) EMI simulation \cite{tveito2021modeling} G) Breast Cancer Cell Growth \cite{lai2021scalable} H) Myocyte mechanics \cite{tveito2021modeling}. These promising technologies can be integrated with data-driven modelling techniques to enhance medical impact.}
%\end{wrapfigure}
% 
%
%\paragraphtitle{Biophysical Modelling:}
%At first, I will seek to discover biophysical relationships within existing continuum mechanics frameworks such as the bidomain model for electrophysiology. I will use large medical imaging datasets from my collaborators for this purpose. This intital work could be extended to consider the EMI framework \cite{tveito2021modeling}, which explicitly models the cellular membrane, and thereby provides a rich space of biophysical relationships that can be explored with medical data.
% 
%\paragraphtitle{High Performance Computing:}
%Automated biophysical discovery will require integrating multiscale biophysical modelling with cutting edge machine learning and scaling the computations to large medical imaging datasets. This will therefore present a substantial high performance computing challenge, as advanced machine learning algorithms and multiscale biophysics models are typically run on 
%seperate pieces of hardware (GPU and CPU respectively). Advancements in high performance computing will be required. Here Simula's eX3 Infrastructure and work on Graphcore IPU are very promising as they can potentially execute biophysical and machine learning computations on the same platform.
%


%\thispagestyle{fancy}
%\pagestyle{fancy }


\footnotesize
\bibliographystyle{vancouver}
\newpage
\bibliography{references}
\end{document}